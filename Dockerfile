FROM node:10.15.1

WORKDIR /app
COPY . .

RUN npm install

CMD node index.js