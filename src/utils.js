export function randomFloat(min, max) {
  return Number((Math.random() * (max - min) + min).toFixed(2))
}
