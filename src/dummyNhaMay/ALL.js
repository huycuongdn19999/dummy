export default {
  ['KK_CCBVMT']: {
    ['PM10']: {
      min: 17,
      max: 50,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
      status: 2,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_HangDau']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 1,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_HoanKiem']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 95,
      max: 97,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 50,
      max: 60,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
      status: 1,
    },
  },
  ['KK_KimLien']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 150,
      max: 170,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['T9']: {
    ['COD']: {
      min: 140,
      max: 150,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_BTL']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_MyDinh']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_TanMai']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_TayMo']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_ThanhCong']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_NamSon']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 0,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['French_Embassy']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
  ['KK_NamSon2']: {
    ['PM10']: {
      min: 17,
      max: 50,
      status: 2,
    },
    ['PM2_5']: {
      min: 10,
      max: 30,
    },
    ['NO2']: {
      min: 4,
      max: 20,
    },
    ['NO']: {
      min: 3,
      max: 30,
    },
    ['NOx']: {
      min: 10,
      max: 50,
    },
    ['CO']: {
      min: 10,
      max: 1000,
    },
    ['SO2']: {
      min: 0.5,
      max: 10,
    },
    ['O3']: {
      min: 1,
      max: 10,
    },
    ['Temp']: {
      min: 20,
      max: 40,
    },
    ['Do_Am']: {
      min: 50,
      max: 60,
    },
  },
}
