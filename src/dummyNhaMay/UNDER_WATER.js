export default {
  ['NT_VinNuocNgam_HUDU']: {
    ['Flow_NuocNgam']: {
      min: 6,
      max: 9.5,
    },
    ['Level_Dong']: {
      min: 20,
      max: 40,
    },
    ['Level_Tinh']: {
      min: 0.01,
      max: 2,
    },
  },
  ['NT_VinNuocNgam_NGQU']: {
    ['Flow_NuocNgam']: {
      min: 6,
      max: 9.5,
    },
    ['Level_Dong']: {
      min: 20,
      max: 40,
    },
    ['Level_Tinh']: {
      min: 0.01,
      max: 2,
    },
  },
  ['NT_VinNuocNgam_BBOI']: {
    ['Flow_NuocNgam']: {
      min: 6,
      max: 9.5,
      status: 2,
    },
    ['Level_Dong']: {
      min: 20,
      max: 40,
    },
    ['Level_Tinh']: {
      min: 0.01,
      max: 2,
    },
  },
  ['NT_VinNuocNgam_TCAT']: {
    ['Flow_NuocNgam']: {
      min: 6,
      max: 9.5,
    },
    ['Level_Dong']: {
      min: 20,
      max: 40,
    },
    ['Level_Tinh']: {
      min: 0.01,
      max: 2,
    },
  },
  ['University_Hospital']: {
    ['FLOW1']: {
      min: 6,
      max: 9.5,
    },
    ['LEVL-1']: {
      min: 20,
      max: 40,
    },
    ['FLOW2']: {
      min: 20,
      max: 40,
    },
    ['LEVL-2']: {
      min: 20,
      max: 40,
    },
    ['FLOW3']: {
      min: 20,
      max: 40,
    },
    ['LEVL-3']: {
      min: 20,
      max: 40,
    },
    ['LEVL-4']: {
      min: 20,
      max: 40,
    },
    ['FLOW4']: {
      min: 0.01,
      max: 2,
    },
  },
}
