import ALL from './ALL'
import NM from './NM'
import UNDER_WATER from './UNDER_WATER'

export default {
  ...ALL,
  ...NM,
  ...UNDER_WATER
}