import StationAutoModel from '../models/StationAuto'
import getDataStationAutoModel from '../models/getDataStationAuto'
import DUMMY_ALL_STATION from '../dummyNhaMay/ALL'
import { randomFloat } from '../utils'
import moment from 'moment'
import { pushNotification } from './pushNotification'
import warningLevels from '../constants/warningLevels'
import { repleaceMeasureLogApi } from './../api/MonitoringApi'

export default async function lamviec(organization) {
  const stationData = await StationAutoModel.find({ 'removeStatus.allowed': false })
  // return console.log(stationData.length)
  stationData.map(async station => {
    const dummyMau = DUMMY_ALL_STATION[station.key]

    if (!dummyMau) return console.log(`Tram ${station.key} chua cau hinh dummy`)

    const DataStatonModel = getDataStationAutoModel(station.key)

    const dataImport = await getGendata(station.measuringList, dummyMau)

    station.lastLog = dataImport
    station.stationName = station.name
    await station.save()
    // console.log('station',station)
    // await DataStatonModel.create({
    //   ...dataImport,

    // })
    const dataPushNoti = {
      organization,
      station,
      data: dataImport
    }
    // console.log('dataImport',dataImport)
    pushNotification(dataPushNoti, dataImport.receivedAt)
    console.log(`Tram ${station.key} imported`)
  })
}

function convertGioiHan(val) {
  if (val === null || val) return undefined
}

async function getGendata(measuringList, dummyMau) {

  let measuringLogs = {}
  const promiseGenMeasureLogs = measuringList.map(async mea => {
    const resMeasure = {}
    const dummyMeasure = dummyMau[mea.key]
    if (!dummyMeasure) return

    resMeasure.value = randomFloat(dummyMeasure.min, dummyMeasure.max)
    resMeasure.statusDevice = 0
    resMeasure.minLimit = mea.minLimit
    resMeasure.maxLimit = mea.maxLimit
    resMeasure.minTend = mea.minTend
    resMeasure.maxTend = mea.maxTend
    resMeasure.unit = mea.unit

    let maxLimit = undefined
    let minLimit = undefined
    let maxTend = undefined
    let minTend = undefined

    // NOTE measuring la thong so trong config tram
    if (resMeasure.maxLimit || resMeasure.maxLimit === 0) {
      maxLimit = mea.maxLimit
    }
    if (resMeasure.minLimit || resMeasure.minLimit === 0) {
      minLimit = mea.minLimit
    }
    if (resMeasure.maxTend || resMeasure.maxTend === 0) {
      maxTend = mea.maxTend
    }
    if (resMeasure.minTend || resMeasure.minTend === 0) {
      minTend = mea.minTend
    }


    // NOTE: logic check trạng thái của dữ liệu sẽ được xử lý bên service monitoring
    // if (resMeasure.value < minLimit || resMeasure.value > maxLimit) {
    //   resMeasure.warningLevel = warningLevels.EXCEEDED
    // } else if (resMeasure.value < minTend || resMeasure.value > maxTend) {
    //   resMeasure.warningLevel = warningLevels.EXCEEDED_PREPARING
    // }
    const resMeasureWithWarningLevel = await repleaceMeasureLogApi(resMeasure)
    resMeasure.warningLevel = resMeasureWithWarningLevel.warningLevel

    if (dummyMeasure.status) resMeasure.statusDevice = dummyMeasure.status

    measuringLogs[mea.key] = resMeasure
  })
  await Promise.all(promiseGenMeasureLogs)
  return {
    receivedAt: moment()
      .startOf('minute')
      .toDate(),
    measuringLogs
  }
}
