import { SERVICE_MONITORING } from './../config'
import { postFetch } from './../utils/fetch'

export const repleaceMeasureLogApi = async ({ minLimit, maxLimit, minTend, maxTend, value }) => {
  return postFetch(`${SERVICE_MONITORING}/replace-measurelog`, { measureLog: { minLimit, maxLimit, minTend, maxTend, value } })
}
export const checkAndPushNotification = async ({ organization, station, data }) => {
  // console.log(JSON.stringify({ organization, station, data }))
  return postFetch(`${SERVICE_MONITORING}/check-push-notification`, { organization, station, data })
}