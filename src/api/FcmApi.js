const config = require('../config');
const axios = require('axios');
const moment = require('moment')
const _ = require('lodash');
const LABELS = require('../constants/labels')
const NOTIFY = require('../constants/notification')
const TIME = require('../constants/time')

const FcmService = axios.create({
  // baseURL: 'http://localhost:5008' + '/fcm-notification/', //
  baseURL: config.FCM_API + '/fcm-notification/',
  headers: {
    'Content-Type': 'application/json',
    'secret-token': config.SECRET_TOKEN
  },
});

async function addNotiByOrganizationApi(organizationId, contentNoti, station) {
  try {
    const response = await FcmService.post(`/${organizationId}`, contentNoti);
    console.log(LABELS.APP, 'push notification thanh cong'.green)
    return response.data;
  } catch (error) {
    console.log(LABELS.APP, 'push notification thanh cong'.red)
    console.log(LABELS.APP, error.message.red)
  }
}

const SHORT_BODY = {
  [NOTIFY.SENSOR_ERROR]: 'Sensor Error',
  [NOTIFY.SENSOR_GOOD]: 'Sensor Good again',
  [NOTIFY.DATA_EXCEEDED]: `Data Over-range`,
}
// type notification
function getDataForNoti({ 
  type, 
  station, 
  plaintext = '',
  rawData = {},
  dataFilter = [], 
  exceedArr = [], 
  prepareExceedArr = []
}) {
  
  /* NOTE   cấu trúc của object result */
  let result = {
    type: type,                         // @dedicated: trước đây dùng để phần tab, bây giờ dùng status
    dataFilter: dataFilter,             // @dedicated
    status: type,                       // hiển thị loại thông báo
    title: station.stationName,         // station name
    station_id: station._id, 
    short_body: SHORT_BODY[type],       // dùng cho fcm trên mobile
    full_body: plaintext,               // dùng cho fcm trên web
    measurings: dataFilter,             // measuring khong co unit
    receivedAt: moment(),               // thời gian tại thời điểm watchman xử lý
    rawAt: moment(rawData.receivedAt)   // thời gian gốc của data logger trong file gởi về
  };

  switch (type) {
    case NOTIFY.DATA_EXCEEDED: {
      /* 
        loại này có 2 dòng
        dòng đầu hiển thị ds các measure vượt ngưỡng
        dòng hai hiển thị ds các measure chuẩn bị vượt ngưỡng
      */
      let exceed_line = break_line = prepare_exceed_line = ''
      if (exceedArr.length > 0) exceed_line = _getMessExceed(exceedArr);
      if (prepareExceedArr.length > 0) {
        prepare_exceed_line = _getMessExceed(prepareExceedArr);
      }

      result.full_body = exceed_line + break_line + prepare_exceed_line
      break;
    }

    default: break;
  }

  return result;
}

/* return string với measauring and unit
 vd: FLOW (mg/l), ...
*/
const _getMessExceed = (arrExceed = []) => {
  const result = arrExceed.map( item => {
    return `${item.key} ${item.value} ${item.unitStr}`
  })
  return result.join(', ')
};

module.exports = {
  addNotiByOrganizationApi,
  getDataForNoti
};
