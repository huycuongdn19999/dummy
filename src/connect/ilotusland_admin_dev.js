const mongoose = require('mongoose');
const { DB_ADMIN } = require('../config');

var conn = mongoose.createConnection(DB_ADMIN.uri, {
  user: DB_ADMIN.user,
  pass: DB_ADMIN.pass,
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 5000 // Reconnect every 500ms
});

conn.on('error', err => console.log(err));
conn.once('open', () => {
  console.log(DB_ADMIN.uri + ' connect success full');
});

module.exports = exports = conn;
