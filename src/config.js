if (process.env.isDev) {
  require('dotenv').config()
}

export const DB = {
  uri: process.env.MONGO_URI,
  user: process.env.MONGO_USER,
  pass: process.env.MONGO_PASS,
};

export const DB_ADMIN = {
  uri: process.env.DB_ADMIN_URI,
  user: process.env.DB_ADMIN_USER,
  pass: process.env.DB_ADMIN_PASS,
}

export const DB_ORGANIZATION_NAME = process.env.DB_ORGANIZATION_NAME
export const SECRET_TOKEN = process.env.SECRET_TOKEN
export const FCM_API = process.env.FCM_API
export const NOTIFY_FROM = process.env.NOTIFY_FROM
export const DEFAULT_DUMMY_FREQUENCY = process.env.DEFAULT_DUMMY_FREQUENCY || 5
export const SERVICE_MONITORING = process.env.SERVICE_MONITORING
