const LABELS = {
	APP:      '|    APP   | '.blue,
	WATCHMAN: '| WATCHMAN | '.red,
	SCHEDULE: '| SCHEDULE | '.yellow
  }
  
  module.exports = LABELS