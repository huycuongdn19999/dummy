module.exports = {
	GOOD: 0,
	HIEU_CHUAN: 1,
	ERROR: 2
  }
  
  const ENUM_STATUS_DEVICE = {
	'0': "SENSOR_GOOD",
	'1': "SENSOR_ADJUST", // hiệu chuẩn
	'2': "SENSOR_ERROR",
	"SENSOR_GOOD": '0',
	"SENSOR_ADJUST": '1', // hiệu chuẩn
	"SENSOR_ERROR": '2',
  }
  
  module.exports.ENUM_STATUS_DEVICE = ENUM_STATUS_DEVICE