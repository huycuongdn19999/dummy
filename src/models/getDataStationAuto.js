import mongoose from 'mongoose'

const prefix = 'data-station-'

const Schema = new mongoose.Schema({
  receivedAt: { type: Date, default: Date.now },
  measuringLogs: Object,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
})

export default function getDataStationAuto(stationKey) {
  const modelName = prefix + stationKey
  if (mongoose.connection.models && mongoose.connection.models[modelName]) {
    return mongoose.connection.models[modelName]
  } else {
    return mongoose.connection.model(modelName, Schema)
  }
}
