const mongoose = require('mongoose');
const { ilotusland_admin_dev } = require('../connect');

const Type = mongoose.SchemaTypes;

const CacheNotificationPush = new mongoose.Schema({
  organizationId: { type: Type.ObjectId },
  stationId: { type: Type.ObjectId },
  measure: String,
  measureList: [String],
  type: String,
  status: String,
  timePush: { type: Date, expires: '60m', default: Date.now },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

CacheNotificationPush.index({ organizationId: 1, stationId: 1, type: 1, measure:1 }, { unique: true });
module.exports = ilotusland_admin_dev.model('cachenotificationpushs', CacheNotificationPush);
