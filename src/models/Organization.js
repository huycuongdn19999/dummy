const mongoose = require('mongoose');
const { ilotusland_admin_dev } = require('../connect');

const Organization = new mongoose.Schema({
  name: { type: String, required: true },
  address: String,
  phone: Object,
  databaseInfo: { type: Object, required: true },
  languageLocales: Array,
  menu: Object,
  packageInfo: Object,
  dbExisted: { type: Boolean, default: true },
  industry: String,
  ftpPath: String,
  removeStatus: {
    type: Object,
    default: { allowed: false, removeAt: null }
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

module.exports = ilotusland_admin_dev.model('organizations', Organization);
